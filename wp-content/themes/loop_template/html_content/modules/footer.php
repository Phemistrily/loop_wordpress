
<?php
echo'

    <footer itemscope itemtype="http://schema.org/WPFooter">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <img class="logo" src="dist/images/loop-logo-footer.svg" alt="logo">
          </div>

          <div class="col-md-4" >
            <span class="title">Kontakt</span>
            <div>
              <address>
                <p><time>Pon-Pt 9:00-17:00</time></p>
                <p>tel. <span><a href="tel:+792880991">792-880-991</a></span></p>
                <p><a href="mailto:biuro@pos-loop.pl">biuro@pos-loop.pl</a></p>
                <p>ul. Jana Sas-Zubrzyckiego 3/55</p>
                <p>30-611 <span>Kraków</span></p>
              </adress>
              <p>(przed przyjazdem prośba o kontakt telefoniczny)</p>
            </div>
            <a href="https://www.facebook.com/posloop/" target="_blank"><img src="dist/images/facebook-footer.svg"></a>
          </div>

          <div class="col-md-4">
            <span class="title">Strony</span>
              <h2><a href="#">Kasy drukarki fiskalne online</a></h2>
              <h2><a href="#">Kasy i drukarki fiskalne</a></h2>
              <h2><a href="#">Urządzenia niefiskalne</a></h2>
              <h2><a href="#">Terminale płatnicze</a><h2>
              <h2><a href="#">Przywoływacz kelnerski</a></h2>
              <h2><a href="#">Oprogramowanie</a></h2>
              <h2><a href="#">Kontakt</a></h2>
              <h2><a href="#">Polityka prywatności</a></h2>
              <h2><a href="#">Polityka plików cookies</a></h2>
          </div>
        </div>
      </div>
    </footer>
    '?>
    
    <script src="dist/js/classie.js"></script>
    <script src="dist/js/dummydata.js"></script>
    <script src="dist/js/main.js"></script>
    <script src="dist/js/lightgallery.min.js"></script>
    <script src="dist/js/lg-thumbnail.js"></script>
    <script src="dist/js/lg-fullscreen.js"></script>

    <script src="dist/js/swiper.min.js"></script>
    <script src="dist/js/all.min.js"></script>
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmwQrDUfWlp8dEKU8PNAwpwa7PLPKzlCc&callback=initMap">
    </script>
    
<?php
echo'

  </body>
</html>
'?>

