<?php include 'modules\head.php'; ?>
<?php include 'modules\menu.php'; ?>
<?php include 'modules\baner.php'; ?>

<main class="privacy-policy">
  <div class="container">

      <p class="superheader">OSTATNIA MODYFIKACJA 2019<p>
      <p class="header">POLITYKA PRYWATNOŚCI</p>
  
      <div class="text">
      <p>
        <b>Czas retencji</b><br>
        Dane są przechowywane przez okres niezbędny do świadczenia usługi żądanej przez Użytkownika, lub też przez okres określony przez cele opisane w niniejszym dokumencie. Użytkownik może zawsze poprosić Administratora Danych o zawieszenie lub usunięcie danych.
      </p>
      <p>
        <br><br><b>Zbierane typy danych</b><br>
        Wśród typów danych osobowych, które zbiera ta aplikacja, samodzielnie lub za pośrednictwem osób trzecich, są: Cookie, Dane o wykorzystaniu
        Inne zgromadzone Dane Osobowe mogą być opisane w innych sekcjach niniejszej polityki prywatności lub kontekstowo, za pomocą dedykowanego tekstu wyświetlanego przy gromadzeniu danych. Dane osobowe mogą być swobodnie dostarczane przez Użytkownika albo automatycznie zbierane podczas korzystania z tej Aplikacji. 
        Jakiekolwiek korzystanie z plików plików cookie – lub innych narzędzi monitorowania – przez tę Aplikację lub przez właścicieli usług stron trzecich wykorzystywanych przez tę Aplikację, o ile nie zaznaczono inaczej, służy do identyfikacji Użytkowników i zapamiętywania ich preferencji, wyłącznie w celu świadczenia usług wymaganych przez Użytkownika. Nie udostępnienie niektórych danych osobowych może Aplikacji świadczenie usług. 
        Użytkownik ponosi odpowiedzialność za Dane Osobowe stron trzecich publikowane lub udostępniane przez tę Aplikację i deklaruje prawo do komunikowania się z nimi lub nadawania do nich, zwalniając Administratora Danych z wszelkiej odpowiedzialności.
      </p>
      <p>
        <br><br><b>Miejsce</b><br>
        Dane są przetwarzane w biurach Administratora Danych i w innych miejscach, w których znajdują się związane z przetwarzaniem strony. Aby uzyskać więcej informacji, należy skontaktować się z Administratorem Danych.
      </p>

      <p>
      <br><br><b>Metody przetwarzania</b><br>
        Administrator Danych przetwarza Dane Użytkowników w sposób prawidłowy i podejmuje odpowiednie środki bezpieczeństwa w celu ochrony przed nieuprawnionym dostępem, ujawnieniem, modyfikacją lub nieuprawnionym zniszczeniem danych. 
        Przetwarzanie danych odbywa się za pomocą komputerów i/lub narzędzi IT, zgodnie z procedurami organizacyjnymi i sposobami ściśle związanymi ze wskazanymi celami. Poza Administratorem Danych, w niektórych przypadkach, dane mogą być dostępne dla określonych typów osób odpowiedzialnych za działania serwisu (administrację, sprzedaż, marketing, system prawny, administrację systemu) lub podmiotów zewnętrznych (takich jak dostawcy usług technicznych, listonosze, dostawcy hostingu, firmy informatyczne, agencje komunikacyjne) mianowanych, w razie potrzeby, Procesorami Danych przez Właściciela. Aktualny wykaz tych stron można uzyskać od Administratora Danych w każdej chwili.
      </p>   

      <p>
      <br><br><b>Wykorzystanie zgromadzonych danych</b><br>
        Dane dotyczące Użytkownika są zbierane w celu umożliwienia Aplikacji świadczenia usług, jak również do następujących celów: Analityka. 
        Dane Osobowe wykorzystywane do każdego celu są opisane w poszczególnych sekcjach niniejszego dokumentu.
      </p>

      <p>
        Informacje dotyczące przetwarzania danych osobowych
        Dane osobowe zebrane w następujących celach dla następujących usług:
      </p>

      <p>
      <br><br><b>Analityka</b><br>
        Usługi zawarte w tej sekcji umożliwiają Właścicielowi monitorowanie i analizowanie ruchu w sieci. Mogą być też używane do śledzenia zachowania Użytkownika.
      </p>  
      <p>
      <br><br><b>Google Analytics</b><br>
        Google Analytics to usługa analizy sieci internetowej firmy Google Inc. („Google”). Google wykorzystuje dane zebrane w celu śledzenia i zbadania wykorzystania tej Aplikacji, do przygotowania sprawozdań na temat jej działań i do dzielenia się nimi z innymi usługami Google. Firma Google może wykorzystywać zebrane Dane do kontekstualizacji i personalizacji ogłoszeń z własnej sieci reklamowej.
      </p>
      <p>

        <br><br><b>Administrator Danych (lub Właściciel)</b><br>
        Osoba fizyczna, osoba prawna, administracja publicznej lub inny organ, stowarzyszenie bądź organizacja z prawem, również wspólnie z innym Administratorem Danych, do podejmowania decyzji dotyczących celów i metod przetwarzania Danych Osobowych oraz środki stosowane, w tym środki bezpieczeństwa dotyczące funkcjonowania i korzystania z aplikacji. Administrator Danych jest właścicielem tej aplikacji, chyba, że wskazano inaczej.
      </p>
      </div>
  </div>
</main>

<?php include 'modules\footer.php'; ?>