<?php
get_header();
?>
<main>
    <?php include 'modules\menu.php'; ?>
    <?php include 'modules\homepage-slider.php'; ?>
    <?php include 'modules\text-area.php'; ?>
    <?php include 'modules\testimonials.php'; ?>
    <?php include 'modules\photo-text.php'; ?>

</main>
<?php
get_footer();
