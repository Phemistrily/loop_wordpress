	<!-- Main container -->
	<div class="container">
		<!-- Blueprint header -->
        <div class="top__bar">
            <img src="dist/images/loop_mobile.svg" alt="">
            <button class="action action--open" aria-label="Open Menu"><span class="icon icon--menu"></span></button>
        </div>
            <nav id="ml-menu" class="menu">
			    <button class="action action--close" aria-label="Close Menu"><span class="icon icon--cross"></span></button>

            <div class="menu__wrap">
				<ul data-menu="main" class="menu__level" tabindex="-1" role="menu" aria-label="All">
					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-1" aria-owns="submenu-1" href="#">Kasy i drukarki fiskalne ONLINE</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-2" aria-owns="submenu-2" href="#">Kasy i drukarki fiskalne</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" data-submenu="submenu-3" aria-owns="submenu-3" href="#">Urządzenia niefiskalne</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Terminale płatnicze</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Przywoływacz kelnerski</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Oprogramowanie</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="/contact.php">Kontakt</a></li>

				</ul>
				<!-- Submenu 1 -->
				<ul data-menu="submenu-1" id="submenu-1" class="menu__level" tabindex="-1" role="menu" aria-label="Vegetables">
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy mobilne ONLINE</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy małe ONLINE</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy średnie ONLINE</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy specjalizowane ONLINE</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki sklepowe ONLINE</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki apteczne ONLINE</a></li>

				</ul>
				<!-- Submenu 2 -->
				<ul data-menu="submenu-2" id="submenu-2" class="menu__level" tabindex="-1" role="menu" aria-label="Fruits">
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy mobilne</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy małe</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy średnie</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy systemowe</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kasy specjalizowane</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki mobilne</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki sklepowe</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki apteczne</a></li>
				</ul>

				<!-- Submenu 3 -->
				<ul data-menu="submenu-3" id="submenu-3" class="menu__level" tabindex="-1" role="menu" aria-label="Grains">
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Pos i programy</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Kody kreskowe</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Wagi elektroniczne</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Szufldy</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Drukarki paragonowe</a></li>
                    <li class="menu__item" role="menuitem"><a class="menu__link" href="#">Materiały eksploatacyjne</a></li>
					<li class="menu__item" role="menuitem"><a class="menu__link" href="#">Akcesoria</a></li>
				</ul>
			</div>
		</nav>
		<!-- <div class="content">
			<p class="info">Please choose a category</p> -->
			<!-- Ajax loaded content here -->
		<!-- </div> -->
	</div>
	<!-- /view -->





<nav class="md-visible" itemscope itemtype="http://schema.org/SiteNavigationElement">
    <a href="">
        <img src="dist/images/logo.svg" alt="logo">
    </a>
    <ul>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Kasy i drukarki fiskalne ONLINE</span>
            </a>
            <ul>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy mobilne ONLINE</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy małe ONLINE</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy średnie ONLINE</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy specjalizowane ONLINE</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki sklepowe ONLINE</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki apteczne ONLINE</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Kasy i drukarki fiskalne</span>
            </a>
            <ul>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy mobilne</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy małe</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy średnie</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy systemowe</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kasy specjalizowane</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki mobilne</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki sklepowe</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki apteczne</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Urządzenia niefiskalne</span>
            </a>
            <ul>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Pos i programy</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Kody kreskowe</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Wagi elektroniczne</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Szufldy</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Drukarki paragonowe</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Materiały eksploatacyjne</span>
                    </a>
                </li>
                <li>
                    <a href="#" itemprop="url">
                        <span itemprop="name">Akcesoria</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Terminale płatnicze</span>
            </a>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Przywoływacz kelnerski</span>
            </a>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Oprogramowanie</span>
            </a>
        </li>
        <li>
            <a href="#" itemprop="url">
                <span itemprop="name">Kontakt</span>
            </a>
        </li>
    </ul>
</nav>
