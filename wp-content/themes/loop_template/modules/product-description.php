
<section class="product-description">
    <p class="price-title">Cena netto:</p>
    <span class="discounted-price">1590.00 PLN</span>
    <span class="normal-price">1590.00 PLN</span>
    <div class="product-indexes">
        <span class="product-indexes-title">Indeksy produktu:</span>
        <span>K10BWE000016 biało-pomarańczowa</span>
        <span>K10BWE000017 popielato-zielona</span>
        <span>K10BWE000018 popielato-j. popielata</span>
        <span>K10BWE000020 czarno-pomarańczowa</span>
        <span>K10BWE000021 biało-zielona</span>
    </div>
    <div class="product-rewards">
        <img src="dist/images/pr1.svg">
        <img src="dist/images/pr2.svg">
    </div>
</section>