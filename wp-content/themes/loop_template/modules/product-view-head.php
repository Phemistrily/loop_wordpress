<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loop - Kasy Fiskalne, komputery POS, oprogramowanie do gastronomii</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">

    <!-- favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">



    <!-- facebook -->
    <meta property="og:image:width" content="1099">
    <meta property="og:image:height" content="575">
    <meta property="og:title" content="Loop - kasy fiskalne, komputery POS, oprogramowanie do gastronomii, terminale płanicze, drukarki bonowe">
    <meta property="og:description" content="Wdrażanie system&oacute;w wspierających sprzedaż i kontrolę magazyn&oacute;w/pracownik&oacute;w w restauracjach, pubach, kawiarniach. Sprzedaż urządzeń fiskalnych.">
    <meta property="og:image" content="https://pos-loop.pl/dist/images/og-image.jpg">
    <meta property="og:url" content="https://www.facebook.com/posloop/">

	<!-- food icons -->
	<link rel="stylesheet" type="text/css" href="dist/css/organicfoodicons.css" />
	<!-- demo styles -->
	<link rel="stylesheet" type="text/css" href="dist/css/demo.css" />
	<!-- menu styles -->
    <link rel="stylesheet" type="text/css" href="dist/css/component.css" />

    <!-- lightbox -->
    <link rel="stylesheet" href="dist/css/lightgallery.min.css">




    <!-- slider -->
    <link rel="stylesheet" href="dist/css/swiper.min.css">

    <link rel="stylesheet" type="text/css" href="dist/css/all.min.css">


    <script src="dist/js/modernizr-custom.js"></script>
    
    <script type='application/ld+json'> 
        {
        "@context": "http://www.schema.org",
        "@type": "ComputerStore",
        "name": "Loop",
        "url": "http://pos-loop.pl/",
        "logo": "http://pos-loop.pl/dist/images/logo.svg",
        "image": "http://pos-loop.pl/dist/images/k10_orange04.jpg",
        "description": "Kasy fiskalne, komputery POS, oprogramowanie do gastronomii, terminale płanicze, drukarki bonowe",
        "address": {
            "@type": "PostalAddress",
            "streetAddress": "Jana Sas-Zubrzyckiego, 3/55",
            "addressLocality": "Kraków",
            "addressRegion": "Małopolskie",
            "postalCode": "30-611",
            "addressCountry": "Polska"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": "50.018400",
            "longitude": "19.949720"
        },
        "openingHours": "Mo, Tu, We, Th, Fr 09:00-17:00",
        "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "792880991"
        },
        "telephone": "792880991",
        "priceRange": "0,59zł-5100zł",
        "email": "biuro@pos-loop.pl"

        }
        </script>

    <script type="application/ld+json">
        {
        "@context": "https://schema.org/", 
        "@type": "Product", 
        "name": "nazwa prodktu",
        "image": [
            "https://przykladowy-adres.com/img/zdjeciazgalerii1.jpg",
            "https://przykladowy-adres.com/img/zdjecieagalerii2.jpg"
            ],
        "description": "opis produkty",
        "brand": {
            "@type": "Thing",
            "name": "nazwa marki TU TRZEBA NOWE POLE"
        },
        "offers": {
            "@type": "Offer",
            "priceCurrency": "PLN",
            "price": "2990.50",
            "itemCondition": "http://schema.org/NewCondition",
            "availability": "http://schema.org/InStock",
            "priceValidUntil": "2080-01-05",
            "url": "www.url-strony.pl"
            }
        }
    </script>

</head>
<body itemscope itemtype="http://schema.org/WebPage">
