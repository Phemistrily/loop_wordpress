<?php echo

    '<section class="photo-text">   
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <a href="#">
                        <h2 class="superheader">KASY MOBILNE ONLINE</h2>
                        <h3 class="header">
                            ELZAB K10 ONLINE &sol; BT GPRS
                        </h3>
                        <p>„Najbardziej nowoczesna i ergonomiczna mobilna kasa na rynku. Wytrzymała, zaprojektowana dla wygody użytkownika, intuicyjnie prosta w obsłudze. Przewyższa funkcjonalnością wiele kas jednostanowiskowych. Wszystko to w kompaktowej bryle kojarzącej się raczej z urządzeniem elektronicznym osobistego użytku, niż kasą fiskalną.”
                        </p>
                        <span class="btn btn-big">zobacz</span>
                    </a>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img src="dist/images/photo-text1.svg" alt=""> 
                    </figure>
                </div>
            </div>
            <div class="row">
            <div class="col-md-6">
                <a href="#">
                    <h2 class="superheader">KASY MOBILNE ONLINE</h2>
                    <h3 class="header">
                        ELZAB K10 ONLINE &sol; BT GPRS
                    </h3>

                    <p>„Najbardziej nowoczesna i ergonomiczna mobilna kasa na rynku. Wytrzymała, zaprojektowana dla wygody użytkownika, intuicyjnie prosta w obsłudze. Przewyższa funkcjonalnością wiele kas jednostanowiskowych. Wszystko to w kompaktowej bryle kojarzącej się raczej z urządzeniem elektronicznym osobistego użytku, niż kasą fiskalną.”
                    </p>
                    <span class="btn btn-big">zobacz</span>
                </a>
            </div>

            <div class="col-md-6">
                <figure>
                    <img src="dist/images/photo-text1.svg" alt=""> 
                </figure>
            </div>
        </div>
        </div>
    </section>'


?>