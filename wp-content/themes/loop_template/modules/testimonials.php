<?php echo'

    <section class="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-md-4 item">
                    <div class="circle">
                        <img src="dist/images/testimonials3.svg">
                    </div>
                    <h4>Zakładasz firmę?</h4>
                    <p>
                        Wspólnie dobierzemy odpowiednie narzędzia, które zapewnią szybką i sprawną obsługę Klienta. Pomożemy w rejestracji kasy fiskalnej i uzyskaniu zwrotu za zakup urządzenia fiskalnego.
                    </p>
                </div>
                <div class="col-md-4 item">
                    <div class="circle">
                        <img src="dist/images/testimonials2.svg">
                    </div>
                    <h4>Firma Loop</h4>
                    <p>
                        To lata doświadczeń w branży fiskalnej. Profesjonalne i sprawdzone oprogramowania wspierającego sprzedaż i prowadzenie firmy. Szybka, sprawna i łatwo dostępna pomoc w późniejszych etapach prowadzenia działalności gospodarczej.
                    </p>
                </div>
                <div class="col-md-4 item">
                    <div class="circle">
                        <img src="dist/images/testimonials1.svg">
                    </div>
                    <h4>Instalacja, szkolenie i serwis</h4>
                    <p>
                        Od doboru odpowiedniego sprzętu, czy oprogramowania, poprzez instalacje i pełne szkolenie – zapewniamy kompleksową obsługę. Na każdym etapie prowadzenia działalności staramy się być dostępni i pomocni.
                    </p>
                </div>
            </div>
        </div>
    </section>


'?>


