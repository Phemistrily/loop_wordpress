<?php
echo'

<div class="homepage-slider">
    <!-- Slider main container -->
    <div class="slider swiper-container swiper-container-homepage">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="slider swiper-slide" style="background-image: url(dist/images/slider1.svg)">
                <header itemscope itemtype="http://schema.org/WPHeader">
                    <h1><strong>OPROGRAMOWANIE GASTRONOMICZNE</strong></h1>
                </header>
            </div>
            
            <div class="slider swiper-slide" style="background-image: url(dist/images/slider2.svg)">
                <header itemscope itemtype="http://schema.org/WPHeader">
                    <h1><strong>TERMINALE PŁATNICZE</strong></h1>
                </header>
            </div>
        
            <div class="slider swiper-slide" style="background-image: url(dist/images/slider3.svg)">
                <header itemscope itemtype="http://schema.org/WPHeader">
                    <h1><strong>KASY FISKLANE</strong></h1>
                </header>
            </div>
        </div>
        <!-- If we need pagination -->
        <div class="swiper-pagination"></div>

        <!-- If we need navigation buttons -->
        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>

    </div>
</div>

'?>