<?php include 'modules\head.php'; ?>
    <?php echo'

        <main class="product-lists-page">

    '?>
            <?php include 'modules\menu.php'; ?>
            <?php include 'modules\baner.php'; ?>
            <?php include 'modules\breadcrumbs.php'; ?>
            <?php include 'modules\text-area.php'; ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <?php include 'modules\sidebar.php'; ?>
                    </div>
                    <div class="col-md-9">
                        <?php include 'modules\products-list.php'; ?>
                    </div>
                </div>
            </div>
            

    <?php echo'

        </main>
    '?>

<?php include 'modules\footer.php'; ?>




