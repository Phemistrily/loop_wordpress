<?php
function add_theme_styles() {
  
  wp_enqueue_style( 'organic-icons', get_template_directory_uri(). '/dist/css/organicfoodicons.css');
  wp_enqueue_style( 'demo', get_template_directory_uri(). '/dist/css/demo.css');
  wp_enqueue_style( 'component', get_template_directory_uri(). '/dist/css/component.css');
  wp_enqueue_style( 'style', get_template_directory_uri(). '/dist/css/all.min.css');
  wp_enqueue_style( 'light-gallery', get_template_directory_uri(). '/dist/css/lightgallery.min.css');
  wp_enqueue_style( 'swipper', get_template_directory_uri(). '/dist/css/swiper.min.css');
  wp_enqueue_script( 'jquery', get_template_directory_uri() . '/dist/js/jquery-3.4.1.min.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/dist/js/modernizr-custom.js', array ( ), 1.1, true);
  wp_enqueue_script( 'all', get_template_directory_uri() . '/dist/js/all.min.js', array ( ), 1.1, true);
  
}
add_action( 'wp_enqueue_scripts', 'add_theme_styles' );