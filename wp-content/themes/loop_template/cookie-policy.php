<?php include 'modules\head.php'; ?>
<?php include 'modules\menu.php'; ?>
<?php include 'modules\baner.php'; ?>

<main class="cookie-policy">
  <div class="container">

      <p class="superheader">OSTATNIA MODYFIKACJA 2019<p>
      <p class="header">POLITYKA COOKIES</p>
  
      <div class="text">
        <p>    
            Poniższa Polityka Cookies określa zasady zapisywania i uzyskiwania dostępu do danych na Urządzeniach Użytkowników korzystających z Serwisu do celów świadczenia usług drogą elektroniczną przez Administratora Serwisu.
            <br><br>
            <b>§ 1 DEFINICJE</b><br>
            Serwis - serwis internetowy działający pod adresem https://pos-loop.pl/

            <br>Serwis zewnętrzny - serwis internetowe partnerów, usługodawców lub usługobiorców Administratora

            <br>Administrator - firma Loop Konrad Warmus, prowadząca działalność pod adresem: 32-020 Wieliczka, Długa 23B, woj. małopolskie, o nadanym numerze identyfikacji podatkowej (NIP): 6831970299, o nadanym numerze REGON: 369309705, świadcząca usługi drogą elektroniczną za pośrednictwem Serwisu oraz przechowująca i uzyskująca dostęp do informacji w urządzeniach Użytkownika

            <br>Użytkownik - osba fizyczna, dla której Administrator świadczy usługi drogą elektroniczna za pośrednictwem Serwisu.

            <br>Urządzenie - elektroniczne urządzenie wraz z oprogramowaniem, za pośrednictwem, którego Użytkownik uzyskuje dostęp do Serwisu

            <br>Cookies (ciasteczka) - dane tekstowe gromadzone w formie plików zamieszczanych na Urządzeniu Użytkownika

            <br><br><b>§ 2 RODZAJE COOKIES</b><br>
            Cookies wewnętrzne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przes system teleinformatyczny Serwisu

            <br>Cookies zewnętrzne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez systemy teleinformatyczne Serwisów zewnętrznych

            <br>Cookies sesyjne - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez Serwis podczas jednej sesji danego Urządzenia. Po zakończeniu sesji pliki są usuwane z Urządzenia Użytkownika.

            <br>Cookies trwałe - pliki zamieszczane i odczytywane z Urządzenia Użytkownika przez Serwis do momentu ich ręcznego usunięcia. Pliki nie są usuwane automatycznie po zakończeniu sesji Urządzenia chyba że konfiguracja Urządzenia Użytkownika jest ustawiona na tryb usuwanie plików Cookie po zakończeniu sesji Urządzenia.

            <br><br><b>§ 3 BEZPIECZEŃSTWO</b><br>
            <br> Mechanizmy składowania i odczytu - Mechanizmy składowania i odczytu Cookies nie pozwalają na pobierania jakichkolwiek danych osobowych ani żadnych informacji poufnych z Urządzenia Użytkownika. Przeniesienie na Urządzenie Użytkownika wirusów, koni trojańskich oraz innych robaków jest praktynie niemożliwe.

            <br>Cookie wewnętrzne - zastosowane przez Administratora Cookie wewnętrzne są bezpieczne dla Urządzeń Użytkowników

            <br>Cookie zewnętrzne - za bezpieczeństwo plików Cookie pochodzących od partnerów Serwisu Administrator nie ponosi odpowiedzialności. Lista partnerów zamieszczona jest w dalszej części Polityki Cookie.

            <br><br><b>§ 4 CELE DO KTÓRYCH WYKORZYSTYWANE SĄ PLIKI COOKIE</b><br>
            <br> Usprawnienie i ułatwienie dostępu do Serwisu - Administrator może przechowywać w plikach Cookie informacje o prefernecjach i ustawieniach użytkownika dotyczących Serwisu aby usprawnić, polepszyć i przyśpieszyć świadczenie usług w ramach Serwisu.

            <br><br><b>§ 5 SERWISY ZEWNĘTRZNE</b><br>
            <br>Administrator nie współpracuje z serwisami zewnętrznymi i Serwis nie umieszcza ani nie korzysta z żadnych plików zewnętrznych plików Cookie.

            <br><br><b> § 6 MOŻLIWOŚCI OKREŚLANIA WARUNKÓW PRZECHOWYWANIA I UZYSKIWANIA DOSTĘPU NA URZĄDZENIACH UŻYTKOWNIKA PRZEZ SERWIS</b><br>
            <br>Użytkownik może w dowolnym momencie, samodzielnie zmienić ustawienia dotyczące zapisywania, usuwania oraz dostępu do danych zapisanych plików Cookies

            <br>Informacje o sposobie wyłączenia plików Cookie w najpopularniejszych przeglądarkach komputerowych i urządzeń mobilnych dostępna są na stronie: jak wyłączyć cookie.

            <br>Użytkownik może w dowolnym momencie usunąć wszelkie zapisane do tej pory pliki Cookie korzystając z narzędzi Urządzenia Użytkownika za pośrednictwem którego Użytkowanik korzysta z usług Serwisu.

            <br><br><b>§ 7 WYMAGANIA SERWISU</b><br>
            <br>Ograniczenie zapisu i dostępu do plików Cookie na Urządzeniu Użytkownika może spowodować nieprawidłowe działanie niektórych funkcji Serwisu.

            <br>Administrator nie ponosi żadnej odpowiedzialności za nieprawidłowo działające funkcje Serwisu w przypadku gdy Użytkownik ograniczy w jakikolwiek sposób możliwość zapisywania i odczytu plików Cookie.

            <br><br><b>§ 8 ZMIANY W POLITYCE COOKIE</b><br>
            <br>Administrator zastrzega sobie prawo do dowolnej zmiany niniejszej Polityki Cookie bez konieczności informowania o tym użytkowników.

            <br>Wprowadzone zmiany w Polityce Cookie zawsze będą publikowane na tej stronie.

            <br> Wprowadzone zmiany wchodzą w życie w dniu publikacji Polityki Cookie.
        </p>
      </div>
  </div>
</main>

<?php include 'modules\footer.php'; ?>