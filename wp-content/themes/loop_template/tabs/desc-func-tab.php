<p>
  <b>DOTYKOWA</b><br>

    Mobilna kasa fiskalna ONLINE z dotykową, kontekstową klawiaturą zapewniającą intuicyjną obsługę. ELZAB K10 ONLINE BT/ WiFi/ EX to kolejna kasa z najpopularniejszej na rynku linii mobilnych kas dotykowych ELZAB K10. Dedykowana zarówno do pracy stacjonarnej jak i w terenie, w usługach oraz małych placówkach handlowych.

    Uniwersalna, nowoczesna, solidna.
    Przetestowana w skrajnych warunkach atmosferycznych, zarówno w słońcu, jak i na mrozie. Można ją obsługiwać w zimowych rękawiczkach. Posiada wymienną baterię o dużej pojemności, która pozwala na pracę w terenie (przy naładowanym akumulatorze ok 5 godz.).
    Zapewnia łączność z Centralnym Repozytorium Kas (CRK) za pośrednictwem modułów do komunikacji bezprzewodowej. Standardowo urządzenie wyposażone jest w moduł Bluetooth/ WiFi.
    Komunikacja WiFi odbywa się poprzez posiadaną sieć lokalną lub za pośrednictwem telefonu komórkowego. Do urządzenia można zamontować dodatkowe akcesorium jakim jest moduł Bluetooth/ GPRS. Komunikację umożliwia wtedy karta SIM.
  <br><br>
  <b>Warte podkreślenia</b>
  <ul>
    <li>
        komunikacja z Centralnym Repozytorium Kas
    </li>
    <li>
        kolorowy, dotykowy wyświetlacz umożliwiający pracę w niskich temperaturach (ilość i wielkość klawiszy zmienia się kontekstowo dostosowując do pracy kasjera)
    </li>
    <li>
        tryb usług czasowych do wykorzystania na parkingach, w wypożyczalniach, kręgielniach, parkach rozrywki
    </li>
    <li>
        współpraca z terminalem kart płatniczych, czytnikiem kodów kreskowych i szufladą kasową
    </li>
  </ul>
</p>
    <br>
<blockquote>
    <p>Zdaniem eksperta:
    Najbardziej nowoczesna i ergonomiczna mobilna kasa na rynku. Wytrzymała, zaprojektowana dla wygody użytkownika, intuicyjnie prosta w obsłudze. Przewyższa funkcjonalnością wiele kas jednostanowiskowych. Wszystko to w kompaktowej bryle kojarzącej się raczej z urządzeniem elektronicznym osobistego użytku, niż kasą fiskalną.</p> 							
</blockquote>