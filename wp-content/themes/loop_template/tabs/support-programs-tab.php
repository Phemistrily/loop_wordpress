<div class="table">
    <div class="table-header-row">
        <div class="table-header program-name-header">
            Nazwa programu	
        </div>
        <div class="table-header company-name-header">
            Nazwa firmy	
        </div>
        <div class="table-header company-adress-header">
            Adres firmy
        </div>
    </div>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name company-data-header">
            ERP OPTIMA	
        </div>
        <div class="company-name">
            <p>Comarch</p>
        </div>
        <div class="company-adress">
            <p>ul. ELZAB 1, <br>
            41-813 Zabrze<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            KC-MARKET/ KC-FIRMA		
        </div>
        <div class="company-name">             
            <p>KUCHARSCY</p>
        </div>
        <div class="company-adress">
            <p>ul. Kazimierza Wielkiego 65,<br>
            32-700 Bochnia<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            MiniMag
        </div>
        <div class="company-name">
            <p>Elzab Soft</p>
        </div>
        <div class="company-adress">
            <p>
            ul. ELZAB 1, <br>
            41-813 Zabrze<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            NAVIREO
        </div>
        <div class="company-name">
            <p>InsERT S.A.</p>
        </div>
        <div class="company-adress">
        <p>
            ul. Jerzmanowska 2,<br>
            54-519 Wrocław<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            SMALL BUSINESS
        </div>
        <div class="company-name">
            <p>SYMPLEX</p>
        </div>
        <div class="company-adress">
        <p>
            ul.Witosa 16,<br>
            57-300 Kłodzko<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            SUBIEKT GTSUBIEKT Nexo
        </div>
        <div class="company-name">
            <p>InsERT S.A.</p>
        </div>
        <div class="company-adress">
            <p>
            ul. Jerzmanowska 2,<br>
            54-519 Wrocław<br></p>
        </div>
    </a>



    <a href="www.comarch.pl/erp" target="_blank" class="table-row">
        <div class="program-name">
            System SKLEP
        </div>
        <div class="company-name">
            <p>INFOKOMP</p>
        </div>
        <div class="company-adress">
        <p>
            Harbutowice 223,<br>
            43-430 Skoczów<br></p>
        </div>
    </a>

</div>
